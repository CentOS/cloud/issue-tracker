# Issue tracker for Project Sagano

This is the repo to track work in progress and issues for the Project Sagano.

## Goals

This project's toplevel goal is to create "base" *bootable* container images from Fedora ELN and
CentOS Stream packages.

## Trying it out

See [install.md](doc/install.md).

## Status

This is an in-development project not intended for production use yet.

## Differences from Fedora CoreOS

Fedora CoreOS today is not small; there are multiple reasons for this, but
primarily because it was created in a pre-bootable-container time.  Not everyone wants
e.g. moby-engine.

But going beyond size, the images produced by this project will focus
on a container-native flow.  We will ship a (container) image that does not include Ignition
for example.

## Differences from RHEL CoreOS

We sometimes say that RHEL CoreOS [has FCOS as an upstream](https://github.com/openshift/os/blob/master/docs/faq.md#q-what-is-coreos)
but this is only kind of true; RHEL CoreOS includes a subset of FCOS content,
and is lifecycled with OCP.

An explicit goal of this project is to produce bootable container images
that can be used as *base images* for RHEL CoreOS; for more on this, see e.g.
https://github.com/openshift/os/issues/799

## Differences from RHEL for Edge

It is an explicit goal that Sagano also becomes a "base input" to RHEL for Edge.

## What does Sagano means?

From [Wikipedia](https://en.wikipedia.org/wiki/Bamboo_Forest_(Kyoto,_Japan)):

> Bamboo Forest, Arashiyama Bamboo Grove or Sagano Bamboo Forest, is a natural
> forest of bamboo in Arashiyama, Kyoto, Japan
